# clickverif

 **

### 有攒劲的动画效果，纯前端的点击文字验证工具

** 

![输入图片说明](https://gitee.com/zhaiyuhan/clickverif/raw/master/1662457280982_B7437005-FD87-4097-A714-3DD35EDEF248.png)

![输入图片说明](https://gitee.com/zhaiyuhan/clickverif/raw/master/1662457548492_2F68CEA6-64A4-4463-BCCB-08FA5E3898AB.png)

![输入图片说明](https://gitee.com/zhaiyuhan/clickverif/raw/master/1662457561596_C34CE5A5-0A01-43ae-A293-F1D64BBC43B3.png)


#### 介绍
VUE前端点击汉字验证的插件


#### 软件架构
没啥架构，就是好使


#### 安装教程
npm i clickverif -S

#### 使用说明

```
import clickverif from "clickverif";

//width 你想设置宽度，由着你的性子写
//showWord 需要显示的文字，必须都是!!4个字符!!
//success 成功验证后的回调，一般就是登录发短信
  <div id="app">
    <clickverif
    :width="300"
    :showWord="['一二三四', '五六七八']"
    @success="successDo"
    />
  </div>

```



#### 参与贡献
就我一个人写的

